function oopNasledovanie() {
//103 page
    let person = {
        getGreeting() {
            return "Hello";
        }
    };
    let friend = {
        getGreeting() {
            return super.getGreeting() + ", hi!";
        }
    };
    Object.setPrototypeOf(friend, person);
    console.log(friend.getGreeting());
    console.log(person.getGreeting());
}

function problenES5() {
    //105 page
    'use strict';
    let person = {
        getGreeting() {
            return "Hello";
        }
    };
    let friend = {
        getGreeting() {
            return Object.getPrototypeOf(this).getGreeting.call(this) + ", hi!";
        }
    };

    Object.setPrototypeOf(friend, person);
    let relative = Object.create(friend);
    console.log(friend.getGreeting());
    console.log(person.getGreeting());
    console.log(relative.getGreeting());
}

//destruction

function destruction() {
    //109 page
    let node = {
        type: "Indentifier",
        name: "foo"
    };
    let {type, name} = node;

    console.log(type);
    console.log(name);
}

function destruction2() {
    //110 page
    let node = {
        type: "Indentifier",
        name: "foo"
    };
    let name = {namme1: "foo"};
    ({name} = node);

    console.log(name);
}

function destruction3() {
    //112 page
    let node = {
        type: "Indentifier",
        name: "foo"
    };
    let {name: localname = "bar"} = node;

    console.log(localname);
}

function destruction4() {
    //112 page
    let node = {
        type: "Indentifier",
        name: {nameI: "foo"}
    };
    let {name: {nameI}} = node;
    console.log(name);
    console.log(nameI);
}

function swithTwo() {
    //����� ���������� � ECTMAScript 6
    let a = 1, b = 2;
    [a, b] = [b, a];
    console.log(a);
    console.log(b);
}

//Symbols

function simvolPage() {
    let firstName = Symbol('first name');
    let person = {};
    person[firstName] = "Nicholas";
    console.log("first name" in person);
    console.log(person[firstName]);
    console.log(firstName);
    for (const owner in person) {
        console.log(owner);
    }
}

function useSimvolPage() {
    let firstName = Symbol.for('first name');
    let person = {
        [firstName]: "Nicholas"
    };
    Object.defineProperty(person, firstName, {writable: false});
    let lastName = Symbol('last name');
    Object.defineProperties(person, {
        [lastName]: {
            value: "Zakas",
            writable: false
        }
    });
    debugger
    person[firstName] = "Nicholas1";
    console.log("first name" in person);
    console.log(person[firstName]);
    console.log(person[lastName]);
    for (const owner in person) {
        console.log(owner);
    }
    for (const simvol of Object.getOwnPropertySymbols(person)) {
        console.log(simvol);
        console.log(person[simvol]);
    }
}

function exampleEchangeDefoutOperator() {
    let obj = {
        0: "first",
        1: "last",
        length: 2,
        [Symbol.isConcatSpreadable]: true
    };
    let newArray = ['one'].concat(obj);
    console.log(newArray);
}

function EchangeDefaultType() {
    function Person(name) {
        this.name = name;
    }

    Person.prototype[Symbol.toStringTag] = 'Person';
    let me = new Person("Nicolas");
    console.log(me.toString());
    console.log(Object.prototype.toString.call(me));
}

//maps
function problenOverwiew() {
    let map = Object.create(null);
    let key1 = {};
    let key2 = {};
    map[key1] = "foo";
    console.log(map[key1]);
    console.log(map[key2]);
}

function setForEachExample() {
    let set = new Set([1, 2]);

    set.forEach((value, key, ownerSet) => {
        console.log(key + " " + value);
        console.log(ownerSet === set);
    });
    set.forEach(function (value, key, ownerSet) {
        console.log(key + " " + value);
        console.log(ownerSet === set);
    });
}

function setForEachExample2() {
    let set = new Set([1, 2]);
    let processor = {
        output(value) {
            console.log(value);
        },
        process(dataSet) {
            dataSet.forEach(value => this.output(value));
        }
    };

    processor.process(set);
}

function setAndArrayExample() {
    let set = new Set([1, 2, 3, 4, 5]);
    let array = [...set];
    console.log(array);
}

function print(value) {
    console.log(value);
}

function weakSetExample() {
    let set = new WeakSet(),
        key = {};
    set.add(key);
    print(set.has(key));
    key = null;//������ ����� ������� �� set
}

function mapExample() {
    let map = new Map();
    map.set('name', 'Nicholas');
    map.set('age', 25);

    print(map.size);
    print(map.has('name'));
    print(map.get('name'));

    print(map.has('age'));
    print(map.get('age'));

    map.delete('name');
    print(map.size);
    print(map.has('name'));
    print(map.get('name'));

    map.clear();
    print(map.has('name'));
    print(map.get('name'));
    print(map.has('age'));
    print(map.get('age'));
    print(map.size);
}

function mapExample2() {
    let map = new Map([['name', 'Nicholas'], ['age', 25]]);
    map.forEach((value, key, ownerMap) => {
        print(key + " " + value);
        print(ownerMap === map);
    });
}

function ectma2015HidenPrivateProperty() {
    var Person = (function () {
        var privateData = {},
            privateId = 0;

        function Person(name) {
            Object.defineProperty(this, "_id", {value: privateId++});

            privateData[this._id] = {
                name: name
            };
        }

        Person.prototype.getName = function () {
            return privateData[this._id].name;
        };

        return Person;
        /*
        * ����� ������� ��������� ������ ������� - ������ � privateData ������� �� ��������
        * ������� ������� ����
        * */
    }());
    debugger
}

function hiddenPrivateProperty() {
    let Person = (function () {
        let privateData = new WeakMap();

        function Person(name) {
            privateData.set(this, {name: name});
        }

        Person.prototype.getName = function () {
            return privateData.get(this).name;
        };

        return Person;
    }());

    debugger
}

/*
* iterators and for
* */

function simpleFor() {
    let colors = ['red', 'green', 'blue'];

    for (let i = 0, len = colors.length; i < len; i++) {
        print(colors[i]);
    }
}

function createIteratorExample() {
    function createIterator(items) {
        let i = 0;
        return {
            next() {
                let done = (i >= items.length);
                let value = !done ? items[i++] : undefined;

                return {
                    done,
                    value
                };
            }
        };
    }

    let iterator = createIterator([1, 2, 3]);
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
}

function generatorCreateExample() {
    function* createIterator() {
        yield 1;
        yield 2;
        yield 3;
    }

    let iterator = createIterator();
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
}

function generatorCreateExample2() {
    function* createIterator(items) {
        for (let i = 0; i < items.length; i++) {
            yield items[i];
        }
    }

    let iterator = createIterator([1, 2, 3]);
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
}

function generatorCreateExample3() {
    let createIterator = function* (items) {
        for (let i = 0; i < items.length; i++) {
            yield items[i];
        }
    };

    let iterator = createIterator([1, 2, 3]);
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
}

function generatorCreateExample4() {
    let o = {
        * createIterator(items) {
            for (let i = 0; i < items.length; i++) {
                yield items[i];
            }
        }
    };

    let iterator = o.createIterator([1, 2, 3]);
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
}

function forOfExample() {
    let values = [1, 2, 3];
    for (let num of values) {
        print(num);
    }
}

function createCollection() {
    let collection = {
        items: [],
        * [Symbol.iterator]() {
            for (let item of this.items) {
                yield  item;
            }
        }
    };
    collection.items.push(1);
    collection.items.push(2);
    collection.items.push(3);

    for (let x of collection) {
        print(x);
    }
}


function entitiesExample() {
    let colors = ['red', 'green', 'blue'];
    let tracking = new Set([1234, 5678, 9102]);
    let data = new Map();

    data.set("title", 'ECMAScript 6');
    data.set("format", 'ebook');

    for (let entry of colors.entries()) {
        print(entry);
    }
    for (let entry of tracking.entries()) {
        print(entry);
    }
    for (let entry of data.entries()) {
        print(entry);
    }
}

function valuesExample() {
    'use strict';
    /*
    * page 175 no valid example*/
    let colors = ['red', 'green', 'blue'];
    let tracking = new Set([1234, 5678, 9102]);
    let data = new Map();

    data.set("title", 'ECMAScript 6');
    data.set("format", 'ebook');

    for (let value of colors) {
        print(value);
    }
    for (let value of tracking.values()) {
        print(value);
    }
    for (let value of data.values()) {
        print(value);
    }
}

function keysExample() {
    let colors = ['red', 'green', 'blue'];
    let tracking = new Set([1234, 5678, 9102]);
    let data = new Map();

    data.set("title", 'ECMAScript 6');
    data.set("format", 'ebook');

    for (let value of colors.keys()) {
        print(value);
    }
    for (let value of tracking.keys()) {
        print(value);
    }
    for (let value of data.keys()) {
        print(value);
    }
}

function defaultForOfExample() {
    let colors = ['red', 'green', 'blue'];
    let tracking = new Set([1234, 5678, 9102]);
    let data = new Map();

    data.set("title", 'ECMAScript 6');
    data.set("format", 'ebook');

    for (let value of colors) {
        print(value);
    }
    for (let value of tracking) {
        print(value);
    }
    for (let value of data) {
        print(value);
    }
}


function forOfDectructionExample() {
    let colors = ['red', 'green', 'blue'];
    let tracking = new Set([1234, 5678, 9102]);
    let data = new Map();

    data.set("title", 'ECMAScript 6');
    data.set("format", 'ebook');

    for (let [key, value] of colors.entries()) {
        print('key:' + key);
        print('value:' + value);
    }
    for (let [key, value] of tracking.entries()) {
        print('key:' + key);
        print('value:' + value);
    }
    for (let [key, value] of data.entries()) {
        print('key:' + key);
        print('value:' + value);
    }
}


function argumentsInIterators() {
    function* createIterator() {
        let first = yield 1;
        let second = yield first + 2;
        yield second + 3;
    }

    let iterator = createIterator();
    print(iterator.next());
    print(iterator.next(4));
    print(iterator.next(5));
    print(iterator.next(5));
    print(iterator.next(5));
}

function asinhrIterators() {
    function* createIterator() {
        let first = yield 1;
        let second;
        try {
            second = yield first + 2;
        } catch (ex) {
            second = 6;
        }
        yield second + 3;
    }

    let iterator = createIterator();
    print(iterator.next());
    print(iterator.next(4));
    print(iterator.throw(new Error('boom')));
    print(iterator.next());
    print(iterator.next(5));
}


function delegareGenerators() {
    function* createNumberIterator() {
        yield 1;
        yield 2;
    }

    function* createColorIterator() {
        yield 'red';
        yield 'green';
    }

    function* createCombinedIterator() {
        yield* createNumberIterator();
        yield* createColorIterator();
        yield true;
    }

    let iterator = createCombinedIterator();

    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.next());
    print(iterator.throw(new Error('boom')));
    print(iterator.next(8));
    print(iterator.next(5));
}

function simpleAsinhrone() {
    function run(taskDef) {
        let task = taskDef();
        let result = task.next();

        function step() {
            if (!result.done) {
                result = task.next();
                step();
            }
        }

        step();
    }

    let generator = function* () {
        console.log(1);
        yield;
        console.log(2);
        yield;
        console.log(3);
    };
    run(generator);
}


function simpleAsinhroneByReturnValue() {
    function run(taskDef) {
        let task = taskDef();
        let result = task.next();

        function step() {
            if (!result.done) {
                result = task.next();
                step();
            }
        }

        step();
    }

    run(function* () {
        let value = yield 1;
        console.log(value);
        value = yield value + 3;
        console.log(value);
    });
}

function asuncExamples() {
    function run(taskDef) {
        let task = taskDef();
        let result = task.next();

        function step() {
            if (!result.done) {
                if (typeof result.value === 'function') {
                    result.value(
                        function (err, data) {
                            if (err) {
                                result = task.throw(err);
                                return;
                            }

                            result = task.next(data);
                            step();
                        }
                    );
                } else {
                    result = task.next(result.value);
                    step();
                }
            }
        }

        step();
    }
}

function acuncReadFile() {
    let js = require('fs');

    function readFile(filename) {
        return function (callback) {
            fs.readFile(filename, callback);
        };
    }

    run(function* () {
        let constents = yield readFile('config.json');
        doSomethingWith(constents);
        console.log('Done');
    });
}


//classes

function simpleClassECMA5() {
    function PersonType(name) {
        this.name = name;
    }

    PersonType.prototype.sayName = function () {
        console.log(this.name);
    };
    let person = new PersonType('Nicholas');
    person.sayName();
    print(person instanceof PersonType);
    print(person instanceof Object);
}

function createSimpelClass() {
    class PerconClass {
        constructor(name) {
            this.name = name;
        }

        sayName() {
            print(this.name);
        }
    }

    let person = new PerconClass('Nicholas');
    person.sayName();
    print(person instanceof PerconClass);
    print(person instanceof Object);
    print(typeof PerconClass);
    print(typeof PerconClass.prototype.sayName);
}

function createSimpleClassECMA5() {
    let PersonType2 = (function () {
        'use strict';
        const PersonType2 = function (name) {
            if (typeof new.target === 'undefined') {
                throw new Error('no new use');
            }
            this.name = name;
        };

        Object.defineProperty(PersonType2.prototype, 'sayName', {
            value: () => {
                if (typeof new.target !== 'undefined') {
                    throw new Error('no new use');
                }
                print(this.name);
            },
            enumerable: false,
            writable: true,
            configurable: true
        });

        return PersonType2;
    });
}

function classForFunction() {
    function createObject(classDef) {
        return new classDef();
    }

    let obj = createObject(class {
        sayHi() {
            print('Hi');
        }
    });
    obj.sayHi();
}

function singeltonClass() {

    let obj = new class {
        constructor(name) {
            this.name = name;
        }

        sayName() {
            print(this.name);
        }
    }('Nikolas');
    obj.sayName();
}

function getSetClass() {
    class CustomHTMLElement {
        constructor(element) {
            this.element = element;
        }

        get html() {
            return this.element.innerHTML;
        }

        set html(value) {
            this.element.innerHTML = value;
        }
    }

    debugger
    let descriptor = Object.getOwnPropertyDescriptor(CustomHTMLElement.prototype, 'html');
    print('get' in descriptor);
    print('set' in descriptor);
    print(descriptor.enumerable);
}

function leteralPropertyClass() {
    let propertyName = 'html';

    class CustomHTMLElement {
        constructor(element) {
            this.element = element;
        }

        get [propertyName]() {
            return this.element.innerHTML;
        }

        set [propertyName](value) {
            this.element.innerHTML = value;
        }
    }
}

function classGeneratorMetod() {
    class MyClass {
        * createIterator() {
            yield 1;
            yield 2;
            yield 3;
        }
    }

    let i = new MyClass();
    let iterator = i.createIterator();
}

function classGeneratorMetod2() {
    /*generate error*/
    class Collection {
        constructor() {
            this.items = [];
        }

        * [Symbol.iterator]() {
            yield* this.items.values();
        }
    }

    let collection = new Collection();
    collection.items.push(1);
    collection.items.push(2);
    collection.items.push(3);
    for (let x of collection) {
        print(x);
    }
}


function ectma5staticClass() {
    function PersonType(name) {
        this.name = name;
    }

    PersonType.create = name => new PersonType(name);
    PersonType.prototype.sayName = () => {
        print(this.name);
    };
    let person = PersonType.create('Nicholas');
    person.sayName();
}

function nasledovanieectma5() {
    function Rectangle(lenght, width) {
        this.length = lenght;
        this.width = width;
    }

    Rectangle.prototype.getArea = function () {
        return this.length * this.width;
    };

    function Square(lenght) {
        Rectangle.call(this, lenght, lenght);
    }

    Square.prototype = Object.create(Rectangle.prototype,
        {
            constructor: {
                value: Square,
                enumerable: true,
                writable: true,
                configurable: true
            }
        });

    var square = new Square(3, 3);

    print(square.getArea());
    print(square instanceof Square);
    print(square instanceof Rectangle);
}

function ectma6NasledovanieEquevalent() {
    class Rectangle {
        constructor(lenght, width) {
            this.length = lenght;
            this.width = width;
        }

        getArea() {
            return this.length * this.width;
        }
    }

    class Square extends Rectangle {
        constructor(lenght) {
            super(lenght, lenght);
        }
    }

    const square = new Square(3);

    print(square.getArea());
    print(square instanceof Square);
    print(square instanceof Rectangle);
}

function ectma6NasledovanieEquevalent2() {
    function Rectangle(lenght, width) {
        this.length = lenght;
        this.width = width;
    }

    Rectangle.prototype.getArea = function () {
        return this.length * this.width;
    };

    class Square extends Rectangle {
        constructor(lenght) {
            super(lenght, lenght);
        }
    }

    const square = new Square(3, 3);

    print(square.getArea());
    print(square instanceof Square);
    print(square instanceof Rectangle);
}

function ectma6NasledovanieEquevalent3() {
    function Rectangle(lenght, width) {
        this.length = lenght;
        this.width = width;
    }

    Rectangle.prototype.getArea = function () {
        return this.length * this.width;
    };

    function getBase() {
        return Rectangle;
    }

    class Square extends getBase() {
        constructor(lenght) {
            super(lenght, lenght);
        }
    }

    const square = new Square(3, 3);

    print(square.getArea());
    print(square instanceof Square);
    print(square instanceof Rectangle);
}

function mixinsClassExample() {
    let SerializableMixin = {
        serialaze() {
            return JSON.stringify(this);
        }
    };
    let AreaMixin = {
        getArea() {
            return this.length * this.width;
        }
    };

    function mixin(...mixins) {
        let base = function () {
        };
        Object.assign(base.prototype, ...mixins);
        return base;
    }

    class Square extends mixin(AreaMixin, SerializableMixin) {
        constructor(lenght) {
            super();
            this.length = lenght;
            this.width = lenght;
        }
    }

    let x = new Square(3);
    print(x.getArea());
    print(x.serialaze());
}

function nasledovanieBaseObject() {
    class MyArray extends Array {

    }

    let colors = new MyArray();
    colors[0] = 'red';
    print(colors.length);

    colors.length = 0;
    print(colors);
}

function nasledovalieExample1() {
    class MyClass {
        static get [Symbol.species]() {
            return this;
        }

        constructor(value) {
            this.value = value;
        }

        clone() {
            return new this.constructor[Symbol.species](this.value);
        }
    }

    class MyDeriverClass1 extends MyClass {

    }

    class MyDeriverClass2 extends MyClass {
        static get [Symbol.species]() {
            return MyClass;
        }
    }

    let instance1 = new MyDeriverClass1('foo');
    let clone1 = instance1.clone();
    let instance2 = new MyDeriverClass2('bar');
    let clone2 = instance2.clone();

    print(clone1 instanceof MyClass);
    print(clone1 instanceof MyDeriverClass1);
    print(clone2 instanceof MyClass);
    print(clone2 instanceof MyDeriverClass2);

}


function targetInClasses() {
    class Rectangle {
        constructor(lenght, widht) {
            print(new.target === Rectangle);
            this.length = lenght;
            this.width = widht;
        }
    }

    class Square extends Rectangle {
        constructor(lenght) {
            super(lenght, lenght);
        }
    }

    let obj = new Square(3);
}

function abstractClassExample() {
    class Shape {
        constructor() {
            if (new.target === Shape) {
                throw new Error('This class cannot be instantiated directly');
            }
        }
    }

    class Rectangle extends Shape {
        constructor(lenght, widht) {
            super();
            print(new.target === Rectangle);
            this.length = lenght;
            this.width = widht;
        }
    }

    let x = new Shape();
    let y = new Rectangle(3, 4);
    print(y instanceof Shape);
}

// array


function arrayProblems() {
    let items = new Array(2);
    print(items.length);
    print(items[0]);
    print(items[1]);

    let items1 = new Array("2");
    print(items1.length);
    print(items1[0]);
    print(items1[1]);

    let items2 = new Array(1, 2);
    print('1,2');
    print(items2.length);
    print(items2[0]);
    print(items2[1]);

    let items3 = new Array(3, '2');
    print('3,2');
    print(items3.length);
    print(items3[0]);
    print(items3[1]);
}


function arrayCreateByOf() {
    let items = Array.of(2);
    print('simple 2');
    print(items.length);
    print(items[0]);
    print(items[1]);

    let items1 = Array.of("2");
    print('string 2');
    print(items1.length);
    print(items1[0]);
    print(items1[1]);

    let items2 = Array.of(1, 2);
    print('numbers 1,2');
    print(items2.length);
    print(items2[0]);
    print(items2[1]);

    let items3 = Array.of(3, '2');
    print('number and string 3,2');
    print(items3.length);
    print(items3[0]);
    print(items3[1]);
}

function doSomething() {
    let args = Array.from(arguments);
    print(args);
}


function iterableObjectToArray() {
    let number = {
        * [Symbol.iterator]() {
            yield 1;
            yield 2;
            yield 3;
        }
    };

    let number2 = Array.from(number, (value) => value + 1);
    print(number2);

}

function findExample() {
    let numbers = [25, 30, 35, 40, 45];
    print(numbers.find(n => n > 33));
    print(numbers.findIndex(n => n > 33));
}

function fillArray() {
    let numbers = [1, 2, 3, 4, 5];
    numbers.fill(1);
    print(numbers);
}

function fillArrayRange() {
    let numbers = [1, 2, 3, 4, 5];
    numbers.fill(1, 2);
    print(numbers.toString());
    numbers.fill(0, 1, 3);
    print(numbers.toString());
}

function copyWithinArray() {
    let numbers = [1, 2, 3, 4, 5];
    numbers.copyWithin(2, 0);
    print(numbers.toString());
}

//�������������� �������
function simpleTipArrray() {
    let buffer = new ArrayBuffer(10),
        view1 = new DataView(buffer),
        view2 = new DataView(buffer, 5, 2);

    print(view1.buffer === buffer);
    print(view2.buffer === buffer);
    print(view1.byteOffset);
    print(view2.byteOffset);
    print(view1.byteLength);
    print(view2.byteLength);
}

function typArrayExample() {
    let buffer = new ArrayBuffer(2),
        view = new DataView(buffer);
    view.setInt8(0, 5);
    view.setInt8(1, -1);

    print(view.getInt16(0));
    print(view.getInt8(0));
    print(view.getInt8(1));
}

function typArrayExample2() {
    let buffer = new ArrayBuffer(10),
        view = new Int8Array(buffer);
    view[0] = 5;
    view[1] = 8

    print(view);
    print(view[0]);
    print(view[1]);
}

function typArrayExample3() {
    let insts16 = new Int16Array([25, 50]),
        insts32 = new Int32Array(insts16);
    print(insts16);
    print(insts32);
}

function typArrayExample4() {
    let insts16 = new Int16Array([25, 50]),
        mapped = insts16.map(v => v * 2);
    print(mapped);
}

function typAddNewDataExample4() {
    let insts16 = new Int16Array(4);
    insts16.set([25, 18]);
    insts16.set([80, 100], 2);
    print(insts16);
}

function typAddCreateCopyExample4() {
    let insts16 = new Int16Array([25, 18, 80, 100]);
    let subints1 = insts16.subarray(),
        subints2 = insts16.subarray(2),
        subints3 = insts16.subarray(1, 3);
    print(subints1);
    print(subints2);
    print(subints3);
}

//Promise

function promiseSimpleExample() {
    let promise = new Promise(function (resolve, reject) {
        print('Promise');
        resolve();
    });
    promise.then(function () {
        print('Resolved');
    });
    print("Hi!");
}

function promiseSimplePesolve() {
    let promise = Promise.resolve(42);
    promise.then(value => print(value));
}

function promiseThrowExample() {
    let promise = new Promise((resolve, reject) => {
        throw new Error('Exeptioon!');
    });
    promise.catch(error => console.log(error.message));
}

function promiseInBrauser() {
    let rejected;
    window.onunhandledrejection = function (event) {
        print(event);
        print(rejected === event.promise);
    };

    window.onrejectionhandled = function (event) {
        print(event);
        print(rejected === event.promise);
    };

    rejected = Promise.reject(new Error('Explosion'));
}


function promiseTwoExample() {
    let p1 = new Promise((resolve, reject) => resolve(42));
    p1.then(value => print(value))
        .then(() => print('Finished'));
}

function promiseCatch() {
    let p1 = new Promise((resolve, reject) => {
        throw new Error('Explosion');
    });
    p1.catch(value => {
        print(value);
        throw new Error('Boom!');
    }).catch(value => {
        print(value);
    });
}

function promiseValueForNextPromise() {
    let p = new Promise((resolve, reject) => {
        resolve(42);
    });
    p.then(value => {
        print(value);
        return ++value;
    }).then(value => {
        print(value);
    });
    debugger
}

function promisePromiseForNextPromise() {
    let p1 = new Promise((resolve, reject) => {
        resolve(42);
    });
    let p2 = new Promise((resolve, reject) => {
        reject(42);
    });
    p1.then(value => {
        print(value);
        return p2;
    }).then(value => {
        print(value);// no run
    });
}

function promisePromiseForNextPromiseCatch() {
    let p1 = new Promise((resolve, reject) => {
        resolve(42);
    });
    let p2 = new Promise((resolve, reject) => {
        reject(43);
    });
    p1.then(value => {
        print(value);
        return p2;
    }).catch(value => {
        print(value);
    });
}

function promisePromiseForNextPromisePattern() {
    let p1 = new Promise((resolve, reject) => {
        resolve(42);
    });
    p1.then(value => {
        print(value);
        let p2 = new Promise((resolve, reject) => {
            reject(43);
        });
        return p2;
    }).catch(value => {
        print(value);
    });
}

function promiseArray() {
    let p1 = new Promise((resolve, reject) => {
        resolve(42);
    });
    let p2 = new Promise((resolve, reject) => {
        resolve(43);
    });
    let p3 = new Promise((resolve, reject) => {
        resolve(48);
    });
    let p4 = Promise.all([p1, p2, p3]);

    p4.then(values => {
        print(values);
        for (const v of values) {
            print(v);
        }
    });
}


function promiseRace() {
    let p1 = Promise.resolve(42);
    let p2 = new Promise((resolve, reject) => {
        resolve(43);
    });
    let p3 = new Promise((resolve, reject) => {
        resolve(48);
    });
    let p4 = Promise.race([p1, p2, p3]);

    p4.then(firstResolvePromise => {
        print(firstResolvePromise);
    });
}


// reflectinApi

function arrayBehind() {
    let items = [0, 1, 2, 3];
    debugger
    print(items);

    items.length = 1;
    print(items);
}

function proxySetExample() {
    let target = {
        name: 'target'
    };

    let proxy = new Proxy(target, {
        set(trapTarget, key, value, receiver) {
            if (!trapTarget.hasOwnProperty(key)) {
                if (isNaN(value)) {
                    throw new TypeError("Property must be a number");
                }
            }
            return Reflect.set(trapTarget, key, value, receiver);
        }
    });

    proxy.count = 1;
    print(proxy.count);
    print(target.count);

    proxy.name = 'proxy';
    print(proxy.name);
    print(target.name);
    proxy.anotherName = 'proxy';
}

function proxyGetErrorExample() {
    let proxy = new Proxy({}, {
        get(trapTarget, key, receiver) {
            if (!(key in receiver)) {
                throw new TypeError('Property ' + key + " doesn`t exist.");
            }
            return Reflect.get(trapTarget, key, receiver);
        }
    });
    proxy.name = "proxy";
    print(proxy);
    print(proxy.fdsf);
}

function proxyHasHiden() {
    let target = {
        name: 'name',
        value: 42
    };

    let proxy = new Proxy(target, {
        has(trapTarget, key) {
            if (key === 'value') {
                return false;
            } else {
                return Reflect.has(trapTarget, key);
            }
        }
    });

    print('value' in proxy);
    print('name' in proxy);
    print('toString' in proxy);
}

function proxyDeleteProperty() {
    let target = {
        name: 'name',
        value: 42
    };

    let proxy = new Proxy(target, {
        deleteProperty(trapTarget, key) {
            if (key === 'value') {
                return false;
            } else {
                return Reflect.deleteProperty(trapTarget, key);
            }
        }
    });

    print('value' in proxy);
    print('name' in proxy);

    let result1 = delete proxy.value;
    let result2 = delete proxy.name;
    print('value' in proxy);
    print('name' in proxy);
    print(result1);
    print(result2);
}

function betweenReflectionAndObject() {
    let target1 = {};
    let result1 = Object.setPrototypeOf(target1, {});
    print(result1 === target1);

    let target2 = {};
    let result2 = Reflect.setPrototypeOf(target2, {});
    print(result2 === target2);
    print(result2);
}

function reflectinExtensible() {
    let result1 = Object.isExtensible(1);
    print(result1);
    let r2 = Reflect.isExtensible(1);
    print(r2);
}

function defineErrorSymbolProperty() {
    let proxy = new Proxy({}, {
        defineProperty(trapTarget, key, descriptor) {
            if (typeof key === 'symbol') {
                return false;
            }
            return Reflect.defineProperty(trapTarget, key, descriptor);
        }
    });
    Object.defineProperty(proxy, 'name', {value: 'proxy'});
    print(proxy.name);

    let nameSymbol = Symbol('name');
    Object.defineProperty(proxy, nameSymbol, {value: 'proxy'});
}

function reflectApplyAndConstruct() {
    let target = function () {
        return 42;
    };

    let proxy = new Proxy(target, {
        apply(trapTarget, thisArg, argumentList) {
            return Reflect.apply(trapTarget, thisArg, argumentList);
        },
        construct(trapTarget, thisArg, argumentList) {
            return Reflect.construct(trapTarget, thisArg, argumentList);
        }
    });

    print(typeof proxy);
    print(proxy());
    let instance = new proxy();

    print(instance instanceof proxy);
    print(instance instanceof target);
}

function createFunctionWithParameters() {
    function sum(...values) {
        return values.reduce((previos, current) => previos + current, 0);
    }

    let sumProxy = new Proxy(sum, {
            apply(trapTarget, thisArg, argumentList) {
                argumentList.forEach(arg => {
                    if (typeof arg !== 'number') {
                        throw new TypeError('All arguments must be numbers');
                    }
                });
                return Reflect.apply(trapTarget, thisArg, argumentList);
            },
            construct(trapTarget, argumentList) {
                throw new TypeError('This function can`t be called with new');
            }
        }
    );

    print(sumProxy(1, 2, 3, 4));
    print(sumProxy(1, 2, 3, '4'));
    let result = new sumProxy();
}


function createClassWithoutNew() {
    class Person {
        constructor(name) {
            this.name = name;
        }
    }

    let PersonProxy = new Proxy(Person, {
        apply(trapTarget, thisArg, argumentList) {
            return new trapTarget(...argumentList);
        }
    });

    let me = PersonProxy('Nicholas');
    print(me);
}

function disableProxy() {
    let target = {
        name: 'target'
    };

    let {proxy, revoke} = Proxy.revocable(target, {});
    print(proxy.name);
    revoke();
    print(proxy.name);
}

function proxyClassConstructor() {
    class Thing {
        constructor() {
            return new Proxy(this, {});
        }
    }

    let myThing = new Thing();
    print(myThing instanceof Thing);
    print(myThing);
}

function proxyPrototypePropertyNotFound() {
    let target = {};
    let thing = Object.create(new Proxy(target, {
        get(trapTarget, key, receiver) {
            throw new ReferenceError(`${key} doesn't exist`);
        }
    }));
    thing.name = 'thing';
    print(thing);
    let unknown = thing.unknown;
}

function proxyPrototypePropertySet() {
    let target = {};
    let thing = Object.create(new Proxy(target, {
        set(trapTarget, key, value, receiver) {
            return Reflect.set(trapTarget, key, value, receiver);
        }
    }));
    print(thing.hasOwnProperty('name'));
    thing.name = 'thing';
    print(thing.name);
    print(thing.hasOwnProperty('name'));

    thing.name = 'boo';
    print(thing.name);
}

function proxyPrototypeClassPropertyNotFound() {
    function NoSuchProperty() {

    }

    NoSuchProperty.prototype = new Proxy({}, {
        get(trapTarget, key, receiver) {
            throw new ReferenceError(`${key} doesn't exist`);
        }
    });
    let thing = new NoSuchProperty();
    let name = thing.name;
}


function proxyBaseClassPropertyNotFound() {
    function NoSuchProperty() {

    }

    NoSuchProperty.prototype = new Proxy({}, {
        get(trapTarget, key, receiver) {
            throw new ReferenceError(`${key} doesn't exist`);
        }
    });

    class Square extends NoSuchProperty {
        constructor(lenght, width) {
            super();
            this.length = lenght;
            this.width = width;
        }
    }

    let shape = new Square(2, 6);
    let area1 = shape.length * shape.width;
    print(area1);

    let area2 = shape.length * shape.wdth;
    print(area2);
}

//ECMAScript 2016 (es7)

function decaratorExample() {
    function readonly(target, key, descriptor) {
        descriptor.writable = false;
        return descriptor;
    }

    class Cat {

        //@readonly
        meow() {
            return '_-_ say cat';
        }

        /*
* Object.defineProperty(Cat.prototype, 'meow', {
value: specifiedFunction,
enumerable: false,
configurable: true,
writable: true
});*/
    }

    let garfield = new Cat();
    garfield.meow();
    garfield.meow = () => {
        console.log('I want lasagne!');
    };
}

function decoratorClass() {
    function superhero(target) {
        target.isSuperhero = true;
        target.power = 'flight';
    }

    //@superhero
    class MySuperHero {
    }

    print(MySuperHero.isSuperhero);
}

decoratorClass();



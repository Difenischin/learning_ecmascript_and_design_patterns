function print(text) {
    console.log(text);
}

function AbstraktFactoryPattern() {

    class Button {
        constructor() {
            print('i am button');
        }
    }

    class MacButton extends Button {
        constructor() {
            super()
            print('i am MacButton');
        }
    }

    class WinButton extends Button {
        constructor() {
            super();
            print('i am WinButton');
        }
    }

    class Checkbox {
        constructor() {
            print('i am checkbox');
        }
    }

    class MacCheckbox extends Checkbox {
        constructor() {
            super();
            print('i am MacCheckbox');
        }
    }

    class WinCheckbox extends Checkbox {
        constructor() {
            super();
            print('i am WinCheckbox');
        }
    }

    class GUIFactory {
        constructor() {
            print('i am GUIFactory');
        }

        createButton() {
            return new Button();
        }

        createCheckbox() {
            return new Checkbox();
        }
    }

    class WinFactory extends GUIFactory {
        constructor() {
            super();
            print('i am WinFactory');
        }

        createButton() {
            return new WinButton();
        }

        createCheckbox() {
            return new WinCheckbox();
        }
    }

    class MacFactory extends GUIFactory {
        constructor() {
            super();
            print('i am MacFactory');
        }

        createButton() {
            return new MacButton();
        }

        createCheckbox() {
            return new MacCheckbox();
        }
    }

    const gui = new WinFactory();
    gui.createButton();
    gui.createCheckbox();
}

function BuilderPattern() {
    class Car {

    }

    class Manual {

    }

    class Builder {
        reset() {
        }

        setSeats() {
        }

        setEngine() {
        }

        setTripComputer() {
        }

        setGps() {
        }
    }

    class CarBuilder extends Builder {
        constructor() {
            super();
            this.car = new Car();
        }

        reset() {
            print('CarBuilder reset');
        }

        setSeats() {
        }

        setEngine() {
        }

        setTripComputer() {
        }

        setGps() {
            print('CarBuilder setGps');
        }

        getResult() {
            return this.car;
        }
    }

    class CarManualBuilder extends Builder {
        constructor() {
            super();
            this.manual = new Manual();
        }

        reset() {
            print('CarManualBuilder reset');
        }

        setSeats() {
        }

        setEngine() {
        }

        setTripComputer() {
        }

        setGps() {
            print('CarManualBuilder setGps');
        }

        getResult() {
            return this.manual;
        }
    }

    class Director {
        static create(builder) {
            builder.reset();
            builder.setGps();
        }
    }

    const carBuilder = new CarBuilder();

    Director.create(carBuilder);
    const car = carBuilder.getResult();
    print(car);
    const manualBuilder = new CarManualBuilder();
    Director.create(manualBuilder);
    const manual = manualBuilder.getResult();
    print(manual);
}

function prototypePattern() {
    //�������� �� �� ��� �� �������, ����� �������� ��� �� ��������
    class Shape {
        constructor(shape) {
            if (shape) {
                this.x = shape.x;
                this.y = shape.y;
                this.collor = shape.collor;
            }
        }

        clone() {

        }
    }

    class Regtangle extends Shape {
        constructor(sourse) {
            super(sourse);
            if (sourse) {
                this.width = sourse.width;
                this.hieght = sourse.hieght;
            }
        }

        clone() {
            return new Regtangle(this);
        }
    }

    class Cirle extends Shape {
        constructor(sourse) {
            super(sourse);
            if (sourse) {
                this.radius = sourse.radius;
            }
        }

        clone() {
            return new Cirle(this);
        }
    }

    let shapes = [];

    const circle = new Cirle();
    circle.x = 10;
    circle.y = 20;
    circle.radius = 15;
    shapes.push(circle);

    const rectangle = new Regtangle();
    rectangle.width = 10;
    rectangle.hieght = 20;
    shapes.push(rectangle);

    let shapesCopy = [];
    for (const shape of shapes) {
        shapesCopy.push(shape.clone());
    }
    print(shapes);
    print(shapesCopy);
}

function adapterPattern() {
    class RoundHole {
        constructor(radius) {
            this.radius = radius;
        }

        getRadius() {
            return this.radius;
        }

        fits(roundPeg) {
            return this.getRadius() >= roundPeg.getRadius();
        }
    }

    class RoundPeg {
        constructor(radius) {
            this.radius = radius;
        }

        getRadius() {
            return this.radius;
        }
    }

    class SquarePeg {
        constructor(width) {
            this.width = width;
        }

        getWidth() {
            return this.width;
        }
    }

    class SquarePegAdapter extends RoundPeg {
        constructor(squarePeg) {
            super();
            this.squarePeg = squarePeg;
        }

        getRadius() {
            return Math.sqrt(2 * Math.pow(this.squarePeg.getWidth(), 2)) / 2;
        }
    }

    const hole = new RoundHole(5);
    const rpeg = new RoundPeg(5);
    print(hole.fits(rpeg));

    const smallSqpeg = new SquarePeg(2);
    const largeSqpeg = new SquarePeg(5);
    //print(hole.fits(smallSqpeg));//error

    const smallSqpegAdapter = new SquarePegAdapter(smallSqpeg);
    const largeSqpegAdapter = new SquarePegAdapter(largeSqpeg);
    print(hole.fits(smallSqpegAdapter));
    print(hole.fits(largeSqpegAdapter));
}

function patternMostExample() {
    class Remote {
        constructor(devise) {
            this.devise = devise;
        }

        setVolumeUp() {
            this.devise.setVolumeUp();
        }

        setVolumeDown() {
            this.devise.setVolumeDown();
        }

        getVolume() {
            return this.devise.getVolume();
        }
    }

    class AdvancedRemote extends Remote {
        constructor(devise) {
            super(devise);
        }

        mute() {
            while (this.devise.getVolume() >= 1) {
                this.devise.setVolumeDown();
            }
        }
    }

    class Devise {
        constructor() {

        }

        setVolumeUp() {

        }

        setVolumeDown() {
        }

        getVolume() {

        }
    }

    class Tv extends Devise {
        constructor() {
            super();
            this.im = 'tv';
            this.volume = 0;
        }

        setVolumeUp() {
            this.volume++;
        }

        setVolumeDown() {
            this.volume--;
        }

        getVolume() {
            return this.volume;
        }
    }

    class Radio extends Devise {
        constructor() {
            super();
            this.im = 'radio';
            this.volume = 0;
        }

        setVolumeUp() {
            this.volume += 5;
        }

        setVolumeDown() {
            this.volume -= 5;
        }

        getVolume() {
            return this.volume;
        }
    }

    const tv = new Tv();
    const remote = new AdvancedRemote(tv);
    print(remote.getVolume());
    remote.setVolumeUp();
    print(remote.getVolume());
    remote.mute();
    print(remote.getVolume());
}

function compositePatternExample() {
    class Graphic {
        constructor() {

        }

        draw() {
            print('I am Graphic');
        }
    }

    class Dot extends Graphic {
        constructor() {
            super();
        }

        draw() {
            print('i am Dot');
        }
    }

    class CompoundGraphic extends Graphic {
        constructor() {
            super();
            this.graphics = [];
        }

        add(graphic) {
            this.graphics.push(graphic);
        }

        draw() {
            print('I am CompoundGraphic');
            for (const graph of this.graphics) {
                graph.draw();
            }
        }
    }

    const all = new CompoundGraphic();
    all.add(new Dot());
    all.add(new Dot());
    all.add(new Dot());
    all.draw();
}

function decoratorPatternExample() {
    class DataSourse {
        constructor() {

        }

        write(data) {

        }

        readData() {

        }
    }

    class FileDataSource extends DataSourse {
        constructor(fileName) {
            super();
            this.fileName = fileName;
            print("i am file data source");
        }

        write(data) {

        }

        readData() {

        }
    }

    class DataSourceDecorator extends DataSourse {
        constructor(dataSource) {
            super();
            this.dataSource = dataSource;
            print("i am file data source decorator");
        }

        write(data) {
            this.dataSource.write(data);
        }

        readData() {
            this.dataSource.readData();
        }
    }

    class EncyptionDecorator extends DataSourceDecorator {
        constructor() {
            super();
        }

        write(data) {
            print("i am encyption decorator");
        }

        readData() {
            print("i am encyption decorator");
        }
    }

    class CompossionDecorator extends DataSourceDecorator {
        constructor() {
            super();
        }

        write(data) {
            print("i am Compossion decorator");
        }

        readData() {
            print("i am Compossion decorator");
        }
    }

    const source = new FileDataSource();
    source.write("clean text");
    const compossionSource = new CompossionDecorator(source);
    compossionSource.write("clean text");

    const multiSource = new EncyptionDecorator(compossionSource);
    // ������ � source ��������� ������ �� ��� ��������:
    // Encryption > Compression > FileDataSource

    multiSource.write("clean text");
    // � ���� ���� �������� ������ � ������������� ������.

}

function flyweightPatternExample() {
    class TreeFactory {
        static getTreeType(name, color, texture) {
            if (!TreeFactory.treeTypes) {
                this.treeTypes = [];
            }
            let type = this.treeTypes.filter(treeType => {
                return treeType.name === name && treeType.color === color && treeType.texture === texture;
            });
            if (type.length > 0) {
                return type[0];
            }
            type = new TreeType(name, color, texture);
            this.treeTypes.push(type);
            return type;
        }
    }

    class TreeType {
        constructor(name, color, texture) {
            this.name = name;
            this.color = color;
            this.texture = texture;
        }

        draw(canvas, x, y) {
            print(`I am treeType ${x} , ${y}`);
        }
    }

    class Tree {
        constructor(x, y, treeType) {
            this.x = x;
            this.y = y;
            this.treeType = treeType;
        }

        draw(canvas) {
            this.treeType.draw(canvas, this.x, this.y);
        }
    }

    class Forest {
        constructor() {
            this.trees = [];
        }

        plantTree(x, y, name, color, texture) {
            const type = TreeFactory.getTreeType(name, color, texture);
            const tree = new Tree(x, y, type);
            this.trees.push(tree);

        }

        draw(canvas) {
            for (const tree of this.trees) {
                tree.draw(canvas);
            }
        }
    }

    const forest = new Forest();
    forest.plantTree(1, 2, 'name', 'color', 'texture');
    forest.plantTree(11, 22, 'name', 'color', 'texture');
    forest.plantTree(11, 22, 'name1', 'color1', 'texture1');
    forest.plantTree(3, 3, 'name2', 'color2', 'texture2');
    forest.draw('canvas');
}

flyweightPatternExample();